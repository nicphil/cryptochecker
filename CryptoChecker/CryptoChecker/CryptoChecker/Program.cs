﻿using System;
using System.IO;
using Coinbase;
using Coinbase.Models;
using static System.Threading.Thread;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Timers;
using System.Collections.Generic;
using System.Linq;

namespace CryptoChecker
{
    class Program
    {
        static string _folderPath = "Data";
        static string _filePath = "Data/accounts.json";

        static List<Account> _accounts;
        static CoinbaseClient _coinbaseClient = new CoinbaseClient(new ApiKeyConfig { ApiKey = "TmTu0OGFzaIEffEA", ApiSecret = "Yto8XQN9xcleSGURJCWNERR4cf5kps9n" });
        static List<Money> _moneys = new List<Money>();

        static Timer _timer;

        private static void InitCoinbaseAccounts()
        {

            //// Deserialize if file is found
            //if (!Directory.Exists(_folderPath))
            //{
            //    Directory.CreateDirectory(_folderPath);
            //}

            //if (File.Exists(_filePath))
            //{
            //    string serializedJson = File.ReadAllText(_filePath);
            //    _accounts = JsonSerializer.Deserialize<List<Account>>(serializedJson);
            //}
            //else
            //{
               
            //}

            // Accounts are newly assigned to the static variable _accounts
            _accounts = new List<Account>(_coinbaseClient.Accounts.ListAccountsAsync().Result.Data);

            foreach (Account account in _accounts)
            {
                _moneys.Add(_coinbaseClient.Data.GetSpotPriceAsync(account.Balance.Currency + "-USD", account.UpdatedAt.Value.Date).Result.Data);
            }
        }

        static void Main(string[] args)
        {
            InitCoinbaseAccounts();

            StartBackgroundAssetChecker();

            MaintainGUI();
        }

        private static void StartBackgroundAssetChecker()
        {
            _timer = new Timer();
            _timer.AutoReset = true;
            _timer.Interval = 30000;
            _timer.Elapsed += OnTimer;
            _timer.Start();
        }

        private static void OnTimer(object sender, ElapsedEventArgs e)
        {
            // All 30 seconds
            ResetAccountsIfTransactionWasMade();
            SendMessageIfOwnedMoneysChangedInValue();
        }

        private static void ResetAccountsIfTransactionWasMade()
        {
            List<Account> accounts = new List<Account>(_coinbaseClient.Accounts.ListAccountsAsync().Result.Data);
            for (int i = 0; i < accounts.Count; i++)
            {
                // Check if transaction has been made on an asset
                if (_accounts[i].Balance.Amount != accounts[i].Balance.Amount)
                {
                    // A transaction has been made; the owned coins in an asset-portfolio have been changed
                    _accounts[i] = accounts[i];
                    if (_moneys.FirstOrDefault(x => x.Base == accounts[i].Balance.Currency) == null)
                    {
                        _moneys.Add(_coinbaseClient.Data.GetSpotPriceAsync(accounts[i].Balance.Currency + "-USD").Result.Data);
                    }
                    int index = _moneys.FindIndex(x => x.Base == accounts[i].Balance.Currency);
                    _moneys[index] = _coinbaseClient.Data.GetSpotPriceAsync(accounts[i].Balance.Currency + "-USD").Result.Data;
                }
            }
        }

        private static void SendMessageIfOwnedMoneysChangedInValue()
        {
            List<Money> moneys = new List<Money>();
            // Where is for filtering out the not bought assets
            foreach (Account account in _accounts.Where(x => x.Balance.Amount != 0.0000000000m))
            {
                moneys.Add(_coinbaseClient.Data.GetSpotPriceAsync(account.Balance.Currency + "-USD").Result.Data);
            }


            bool shouldSend = false;
            string header = "";
            string difference = "";
            string content = "";
            foreach (Money money in moneys)
            {
                Money moneyOriginal = _moneys.Find(x => x.Base == money.Base && x.Currency == money.Currency);
                #region SELL-SECTION
                if (moneyOriginal.Amount * 5 <= money.Amount)
                {
                    // MEGA ALERT
                    shouldSend = true;
                    difference = "+500%!!!!!";
                }
                else if (moneyOriginal.Amount * 3 <= money.Amount)
                {
                    // LARGE ALERT
                    shouldSend = true;
                    difference = "+300%!!!";
                }
                else if (moneyOriginal.Amount * 2 <= money.Amount)
                {
                    // BIG ALERT
                    shouldSend = true;
                    difference = "+200%!!";
                }
                else if (moneyOriginal.Amount * 1.5m <= money.Amount)
                {
                    // LETSGO ALERT
                    shouldSend = true;
                    difference = "+50%!";
                }
                else if (moneyOriginal.Amount * 1.2m <= money.Amount)
                {
                    // ALIRGHTY ALERT
                    shouldSend = true;
                    difference = "+20%!";
                }
                else if (moneyOriginal.Amount * 1.1m <= money.Amount)
                {
                    // YE ALERT
                    shouldSend = true;
                    difference = "+10%!";
                }
                #endregion
                #region INVEST-SECTION
                else if (moneyOriginal.Amount * 0.8m >= money.Amount)
                {
                    // MINUS ALERT
                    shouldSend = true;
                    difference = "-20%!";
                }
                #endregion

                if(shouldSend)
                {
                    header = money.Base + " " + difference;
                    content = money.Base + " just went " + difference + "!!";
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(header);
                    Console.WriteLine(content);
                    Console.ResetColor();
                    //SendEmail(header, content);
                }
            }
        }

        private static void SendEmail(object header, object content)
        {
            throw new NotImplementedException();
        }

        private static void MaintainGUI()
        {
            bool stay = true;
            while (stay)
            {
                Console.WriteLine("Welcome to the Coinbase-CryptoChecker!");
                Console.WriteLine("See current assets...................1");
                Console.WriteLine("See coherent changes since Init......2");
                Console.WriteLine("See current balance..................3");
                Console.WriteLine("Exit.................................4");

                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        break;
                    case "2":
                        break;
                    case "3":
                        break;
                    case "4":
                        Console.WriteLine("The application is closing...");
                        Sleep(2000);
                        Exit();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void Exit()
        {
            _timer.Stop();
            //string serializedJson = JsonSerializer.Serialize(_accounts);
            //File.WriteAllText(_filePath, serializedJson);
        }

    }
}
